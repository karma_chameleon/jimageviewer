
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.swing.JComponent;

/**
 *
 * @author karma
 */
public class JImagePanel extends JComponent {

	private BufferedImage img;
	private int iW;
	private int iH;
	private int scaleFactor;
	private int offsetX;
	private int offsetY;
	private int transX;
	private int transY;
	int prevOffX = 0;
	int prevOffY = 0;

	public JImagePanel() {
		
		super();
		scaleFactor = 100;
		this.offsetX = 0;
	}	
	
	public void setTraslation(int x, int y) {
		
		this.transX = x;
		this.transY = y;
	}

	public int[] getTranslation() {
		
		return new int[] {transX, transY};
	}
	
	public int getOffsetY() {
		
		return offsetY;
	}

	public void setOffsetY(int offsetY) {

		this.offsetY = offsetY;
	}

	public int getOffsetX() {
		
		return offsetX;
	}

	public void setOffsetX(int offsetX) {

		this.offsetX = offsetX;
	}
	
	/**
	 * Sets the desired width and height of the image to display.
	 * 
	 * @param w width in pixels
	 * @param h height in pixels
	 */	
	public void setImageSize(int w, int h) {
		
		if (w <= scaleFactor) {
			
			return;
		}
		
		this.iW = w;
		this.iH = h;
		this.transX = (-iW / 2) + prevOffX;  
		this.transY = (-iH / 2) + prevOffY;
	}
	
	public BufferedImage getImage() {
		
		return img;
	}
	
	/**
	 * Sets the image that this component will display
	 * @param img 
	 */
	public void setImage(BufferedImage img) {
		
		this.img = img;
		this.iW = img.getWidth();
		this.iH = img.getHeight();
		this.transX = -iW / 2;
		this.transY = -iH / 2;
		this.repaint();
	}

	/**
	 * Gets the width and height of the image that the component is currently displaying
	 * @return an integer array containing the width in position [0] and the height in position [1]. 
	 */	
	public int[] getImageSize() {

		return new int[] {this.iW, this.iH};
	}
	
	@Override
	protected void paintComponent(Graphics grphcs) {
		
		super.paintComponent(grphcs);

		Graphics2D g2d = (Graphics2D) grphcs;
		
		int winW = this.getWidth();
		int winH = this.getHeight();
		
		g2d.translate(transX + offsetX, transY + offsetY);
		g2d.drawImage(img, winW/2, winH/2, iW, iH, null);
	}
}
