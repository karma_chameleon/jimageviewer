
import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import javax.swing.JPanel;

/**
 *
 * @author karma
 */
public class backgroundPanel extends JPanel {

	@Override
	protected void paintComponent(Graphics grphcs) {

		Graphics2D g2d = (Graphics2D) grphcs;
		super.paintComponent(grphcs);
		int x1 = 0;
		int y1 = 0;
		int x2 = 0;
		int y2 = this.getHeight();
		
		Paint pattern = new GradientPaint(x1,y1, Color.LIGHT_GRAY, x2,y2, Color.DARK_GRAY);
		g2d.setPaint(pattern);
		g2d.fillRect(0, 0, this.getWidth(), this.getHeight());
		
	}
	
}
